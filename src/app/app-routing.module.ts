import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';
import { HomeComponent } from './modules/home/home.component'
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NavsideComponent } from './navside/navside.component';
import { SideBarComponent } from './side-bar/side-bar.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    // children: [
    //   {
    //     path: 'home',
    //     loadChildren: () => import('./modules/home/home.module/')
    //     .then(m=>m.HomeModule)
    //   }
    // ]
  },
  {
    path:'login',component:AuthenticationComponent
  },
  {
    path:'navbar',component:NavBarComponent
  },
  {
    path:'sidebar',component:SideBarComponent
  },
  {
    path:'navside',component:NavsideComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
