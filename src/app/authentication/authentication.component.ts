import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
loginForm!:FormGroup;
error = '';
  constructor(private formBuilder: FormBuilder,
    private router: Router,) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [
        'admin@geniemode.com',
        [Validators.required, Validators.email, Validators.minLength(5)]
      ],
      password: ['admincred', Validators.required]
    });
  }
  get form() {
    return this.loginForm.controls;
  }
  get e()
  {
    return this.loginForm.get("email");
  }

}
